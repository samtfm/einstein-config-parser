const fs = require('fs');
const readline = require('readline');

const BOOL_ALIASES : { [x:string]: boolean } = {
  true: true,
  false: false,
  yes: true,
  no: false,
  on: true,
  off: false,
}

export default async function configParser(filePath: string){
  const readInterface = readline.createInterface({
    input: fs.createReadStream(filePath),
    console: false
  });
  const data: any = {}

  for await (const line of readInterface) {
    // Explicitly ignore comments
    if (line[0] == '#') {
      continue; 
    }

    // parse line into key-value pairs
    const regExMatch = line.match(/(\w+)\s*\=\s*(.*)/)

    // ignore lines that are improperly formatted
    if (!regExMatch || !regExMatch[2]){
      continue;
    }

    const key = regExMatch[1]
    const valString = regExMatch[2]
    let val;

    if (BOOL_ALIASES[valString] !== undefined) {
      val = BOOL_ALIASES[valString];
    } else if (!isNaN(Number(valString))) {
      val = Number(valString);
    } else {
      val = valString;
    }

    data[key] = val;
  }

  return data;
}