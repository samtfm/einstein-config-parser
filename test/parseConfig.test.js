import parseConfig from 'parseConfig';

describe("parseConfig", () => {
  test("it should parse strings into key-value pairs", async () => {
    
    const output = await parseConfig('./test/string-values.txt')
    expect(output).toEqual({
      fruit: 'apple',
      path: '/path/to/file.txt',
      movie_title: '10 things I hate about you',
    });
  })

  test("it should parse yes/no on/off true/false as boolean values", async () => {
    
    const output = await parseConfig('./test/boolean-values.txt')
    expect(output).toEqual({
      test1: true,
      test2: false,
      test3: true,
      test4: false,
      test5: true,
      test6: false,
    });
  })
  test("it should parse numbers into number values", async () => {
    
    const output = await parseConfig('./test/number-values.txt')
    expect(output).toEqual({
      number: 14,
      decimal: .33,
      decimal2: 2.22,
      leadingZero: 2.2,
    });
  })
  test("it should ignore comments", async () => {
    
    const output = await parseConfig('./test/comments.txt')
    expect(output).toEqual({
      test: 'val',
      test2: 'val',
    });
  })

  test("it should parse the example-config", async () => {
    const output = await parseConfig('./test/full-example-config.txt')
    expect(output).toEqual({
      host: 'test.com',
      server_id: 55331,
      server_load_alarm: 2.5,
      user: 'user',
      verbose : true,
      test_mode :  true,
      debug_mode :  false,
      log_file_path :  '/tmp/logfile.log',
      send_notifications :  true,
    });
  })
});
